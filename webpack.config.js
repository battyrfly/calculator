const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const conf = {
	entry: "./src/index.js",
	output: {
		path: path.resolve(__dirname, "./dist"),
		filename: "main.js",
		publicPath: "dist/"
	},
	devServer: {
    host: '0.0.0.0',
		overlay: true
	},
	module: {
		rules: [{
				test: /\.js$/,
				//exclude: '/node_modules/',
				loader: "babel-loader"
			},
			{
				test: /\.styl$/,
				use: [
					MiniCssExtractPlugin.loader,
          // { loader: "style-loader" },
					'css-loader',
					{
						loader: "postcss-loader",
						options: {
							ident: 'postcss',
							plugins: [
								require("postcss-preset-env") ({ browsers: ['cover 99.5%'] }),
								require("cssnano"),
							]
						}
					},
					'stylus-loader',
				]
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: "./css/main.css"
		})
	],
	optimization: {
    minimizer: [new UglifyJsPlugin()],
	},
	resolve: {
		alias: {
			'@': path.resolve(__dirname),
    }
  }
};

module.exports = (env, options) => {
	let production = options.mode === "production";

	conf.devtool = production ? /*'source-map'*/ false : "eval-sourcemap";

	return conf;
};