export const inputDigit = function(digit, calculator) {
	const { displayValue, waitingForSecondOperand, operator } = calculator;

	if (waitingForSecondOperand === true) {
		calculator.displayValue = digit;
    calculator.waitingForSecondOperand = false;
  } else {
    calculator.displayValue = displayValue === '0' ? digit : displayValue + digit;
	}

};


export const inputDecimal = function (dot, calculator) {
	if (calculator.waitingForSecondOperand === true) return;

  if (!calculator.displayValue.includes(dot)) {
    calculator.displayValue += dot;
  }
};