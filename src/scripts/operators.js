import performCalculation from './operations';

export const handleOperator = function (nextOperator, calculator) {
  const { firstOperand, displayValue, waitingForSecondOperand, operator, percent, operands } = calculator;
	const inputValue = displayValue.indexOf('.', displayValue.length-1) === -1 ? parseFloat(displayValue) : false;
	const newOperands = {
		operand: displayValue,
		operator: operator,
		clicked: true
	};

	console.log(calculator)

  if (operator && waitingForSecondOperand && !percent) {
		if (nextOperator !== '%') {
			calculator.operator = nextOperator;
			if (operator !== '=') {
				calculator.operands[calculator.operands.length-1].operator = nextOperator;
				calculator.operands[calculator.operands.length-1].clicked = false;
			} else {
				calculator.operands[0].operator = null;
			}
		} else {
			calculator.operands[0].operator = null;
			calculator.operands[calculator.operands.length-1].clicked = false;
		}
		return;
	}

	console.log(calculator)

	if (inputValue) operands.push( newOperands );

  if (firstOperand === null && inputValue && nextOperator !== '%') {
		calculator.firstOperand = inputValue;
		calculator.displayValue = '';
  } else if (operator) {
		newOperands.operator = operator;

		if (nextOperator === '%') {
			calculator.displayValue = performCalculation[nextOperator](firstOperand, inputValue).toString();
			calculator.percent = true;
			newOperands.operator = '%';
		} else {
			const result = performCalculation[operator](firstOperand, inputValue);

			if (nextOperator === '=') {
				calculator.displayValue = result.toString();
				operands.push({
					operand: result.toString(),
					operator: "=",
					clicked: true
				});
			} else {
				calculator.displayValue = '';
			}

			calculator.firstOperand = result;
		}
	}

	calculator.waitingForSecondOperand 	= inputValue ? true : false;
	calculator.operator 								= inputValue ? nextOperator !== '%' ? nextOperator : operator : null;
	console.log(calculator)
};


export const clearAll = function (calculator) {
	calculator.displayValue = '0';
	calculator.firstOperand = null;
	calculator.waitingForSecondOperand = false;
	calculator.operator = null;
	calculator.percent = null;
	calculator.operands = [];
	calculator.displayOperator.innerHTML = '';
	calculator.display.innerHTML = '';
	calculator.displayHistories.innerHTML = '';
	calculator.displayOperands.innerHTML = '';
};


export const clear = function(calculator) {
	const { waitingForSecondOperand, displayValue } = calculator;
	if (!displayValue.length) calculator.displayValue = '';
	if (!waitingForSecondOperand) {
		calculator.displayValue = calculator.displayValue.substring(0, calculator.displayValue.length-1);
	} else {
		calculator.operands[calculator.operands.length-1].clicked = false;
	}
};

export const negate = function (calculator) {
	const { displayValue } = calculator;
	if (displayValue !== '0') {
		calculator.displayValue = (parseFloat(displayValue) * -1).toString();
	}
};