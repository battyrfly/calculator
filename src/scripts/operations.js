export default {
  '/': (firstOperand, secondOperand) => firstOperand / secondOperand,

  '*': (firstOperand, secondOperand) => firstOperand * secondOperand,

  '+': (firstOperand, secondOperand) => firstOperand + secondOperand,

	'-': (firstOperand, secondOperand) => firstOperand - secondOperand,

  '%': (firstOperand, secondOperand) => firstOperand * secondOperand / 100,

  '=': (firstOperand, secondOperand) => secondOperand
};