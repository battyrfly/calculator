import { inputDigit, inputDecimal } from './scripts/numbers';
import { handleOperator, clearAll, clear, negate } from './scripts/operators';
import performCalculation from './scripts/operations';
import './styles/main.styl';

const calculator = {
  displayValue: '0',
  firstOperand: null,
  waitingForSecondOperand: false,
	operator: null,
	percent: false,
	operands: [],
	displayOperator: document.querySelector('#input span:first-child'),
	display: document.querySelector('#input span:last-child'),
	displayHistories: document.querySelector('#histories'),
	displayOperands: document.querySelector('#operands'),
};

function updateDisplay() {
	const { firstOperand, operands, waitingForSecondOperand, operator, displayValue } = calculator;
	let inputValue = displayValue;
	let operatorValue = operator;

	if (operands.length && operands[operands.length-1].clicked && waitingForSecondOperand) {
		const div = document.createElement("div");
		if (operator !== '=' && firstOperand !== null) {
			div.innerHTML = `
				<span>${operands[operands.length-1].operator ? operands[operands.length-1].operator : ''}</span>
				<span>${operands[operands.length-1].operand}</span>
			`;
			calculator.displayOperands.appendChild( div );
		} else if (operands.length >= 2 || displayValue === 'Infinity') {

			div.classList.add("history");

			for (let index = 0; index < operands.length; index++) {
				div.innerHTML += `
					<div>
						<span>${operands[index].operator ? operands[index].operator : ''}</span>
						<span>${operands[index].operand}</span>
					</div>
				`;
				calculator.displayHistories.appendChild( div );
			}

			inputValue = '0';
			operatorValue = '';
			calculator.displayValue = '0';
			calculator.firstOperand = null;
			calculator.waitingForSecondOperand = false;
			calculator.operator = null;
			calculator.percent = false;
			calculator.operands = [];
			calculator.displayOperands.innerHTML = '';
			calculator.displayHistories.lastChild.scrollIntoView(false);
		}
	}

	calculator.displayOperator.innerHTML = operatorValue;
	calculator.display.innerHTML = inputValue;
}

updateDisplay();

document.querySelector('.app').addEventListener('keydown', (event) => {
	let { key } = event;

	if (key === 'Enter') key = '=';

	if ((/\d/).test(key)) {

		event.preventDefault();
		inputDigit(key, calculator);
		updateDisplay();

	} else if (key in performCalculation) {

		event.preventDefault();
		handleOperator(key, calculator);
		updateDisplay();

	} else if (key === '.') {

		event.preventDefault();
		inputDecimal(key, calculator);
		updateDisplay();

	} else if (key === 'Backspace') {

		event.preventDefault();
		clear(calculator);
		updateDisplay();

	} else if (key === 'Delete') {

		event.preventDefault();
		clearAll(calculator);
		updateDisplay();

	}

});

const keys = document.querySelector('.calculator__keys');
keys.addEventListener('click', (event) => {
	const { target } = event;

  if (!target.matches('button')) {
    return;
  }

  if (target.classList.contains('operator')) {
    handleOperator(target.value, calculator);
		updateDisplay();
    return;
  }

  if (target.classList.contains('negate')) {
    negate(calculator);
		updateDisplay();
    return;
  }

  if (target.classList.contains('decimal')) {
		inputDecimal(target.value, calculator);
		updateDisplay();
    return;
  }

  if (target.classList.contains('clear')) {
    clear(calculator);
		updateDisplay();
    return;
  }

  if (target.classList.contains('all-clear')) {
		clearAll(calculator);
		updateDisplay();
    return;
  }

	inputDigit(target.value, calculator);
	updateDisplay();
});
